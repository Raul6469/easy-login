#!/bin/bash
yes | sdkmanager "platforms;android-${EMULATOR_API_LEVEL}"    # Android platform required by emulator
yes | sdkmanager "platforms;android-${COMPILE_API_LEVEL}"     # Android platform required by compiler
yes | sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" # Android build tools
yes | sdkmanager "${EMULATOR}" # Install emulator system image
# emulator instance
echo no | android create avd --force -n ${EMULATOR_NAME} -k "${EMULATOR}"
# Run emulator in a subshell, this seems to solve the travis QT issue
( cd "$(dirname "$(which emulator)")" && ./emulator -avd ${EMULATOR_NAME} -verbose -show-kernel -selinux permissive -no-audio -no-window -no-boot-anim -wipe-data & )
android-wait-for-emulator
adb shell settings put global window_animation_scale 0 &
adb shell settings put global transition_animation_scale 0 &
adb shell settings put global animator_duration_scale 0 &
sleep 30
adb shell input keyevent 82 &
adb devices