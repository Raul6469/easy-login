package antoinepetetin.fr.easylogin.user


class EasyGoogleUser : EasyUser() {

    var displayName: String? = null
    var photoUrl: String? = null
    var idToken: String? = null
}